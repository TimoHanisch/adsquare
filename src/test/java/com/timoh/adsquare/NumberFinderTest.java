package com.timoh.adsquare;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class NumberFinderTest {

  private final NumberFinder numberFinder = new NumberFinder();

  @Test
  @DisplayName("Should throw an IllegalArgumentException if passed list argument is null.")
  public void findSecondHighestNumber_nullListPassed_throwException() {
    assertThrows(IllegalArgumentException.class,
        () -> numberFinder.findSecondHighestNumber(null));
  }

  @Test
  @DisplayName("Should throw an IllegalArgumentException if passed list size is less than 2.")
  public void findSecondHighestNumber_oneElementListPassed_throwException() {
    final var list = List.of(1);
    assertThrows(IllegalArgumentException.class,
        () -> numberFinder.findSecondHighestNumber(list));
  }

  @Test
  @DisplayName("Should return second highest number when passed list with numbers")
  public void findSecondHighestNumber_listPassed_throwException() {
    final var list = List.of(1, 2, 3);

    final var result = numberFinder.findSecondHighestNumber(list);

    assertThat(result).isEqualTo(2);
  }

  @Test
  @DisplayName("Should return second highest number when passed list with more numbers")
  public void findSecondHighestNumber_biggerListPassed_throwException() {
    final var list = List.of(14, 2, 53, 12, 1, 99, 1294, 12481, 224, 214, 49, 42, 999);

    final var result = numberFinder.findSecondHighestNumber(list);

    assertThat(result).isEqualTo(1294);
  }

  @Test
  @DisplayName("Should return second highest number when passed list with doubled numbers")
  public void findSecondHighestNumber_doubleNumberListPassed_throwException() {
    final var list = List.of(1, 2, 3, 3);

    final var result = numberFinder.findSecondHighestNumber(list);

    assertThat(result).isEqualTo(2);
  }

}
