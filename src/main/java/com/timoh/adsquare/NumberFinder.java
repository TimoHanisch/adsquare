package com.timoh.adsquare;

import java.util.List;

/**
 * Coding Task - adsquare
 *
 * TASK: Please create a new Java project. Then design and implement a function which is accepting a
 * list of integers and returns 2nd biggest element on that list. Note that the list may be long and
 * can contain millions of elements
 *
 * @author Timo Hanisch
 */
public class NumberFinder {

  /**
   * Looks up the 2nd highest number in the list. To do so it iterates through all elements to check
   * this which ends up having a complexity of O(n).
   *
   * Another way to solve this would be to sort the list first and then select the
   * list.get(list.size() - 2) element, this would end up being more complex on the runtime and
   * memory side.
   *
   * @param numbers is the list that should be searched for the 2nd highest element
   * @return the 2nd highest element of the list
   * @throws IllegalArgumentException if size of list is smaller than 2
   */
  public int findSecondHighestNumber(final List<Integer> numbers) throws IllegalArgumentException {
    if (numbers == null || numbers.size() < 2) {
      throw new IllegalArgumentException("List should at least have two elements");
    }
    var highest = Integer.MIN_VALUE;
    var secondHighest = Integer.MIN_VALUE;
    for (Integer number : numbers) {
      if (number > highest) {
        secondHighest = highest;
        highest = number;
      } else if (number > secondHighest && number != highest) {
        // We check for number != highest so that we can handle double occurring highest numbers
        secondHighest = number;
      }
    }
    return secondHighest;
  }
}
