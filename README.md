# adsquare Coding Challenge

### Task
> Please create a new Java project. Then design and implement a function which is accepting a
> list of integers and returns 2nd biggest element on that list. Note that the list may be long and
> can contain millions of elements

### Running Tests

The project uses Maven. To run the test just execute: ```mvn test``` 

### Hint on Performance

This is a simple implementation, to speed things up for extremely large data sets we could break 
down the algorithm to check subsets for the highest and second highest elements. In the final step 
we could check the resulting list of elements for the highest and second highest element.

Additionally, the forEach-loop might be less performant than C-style for-loops.